# ActiveBox

Website theme based on template designed by [Kamal Chaneman](http://www.kamalchaneman.com/).
Created by [Andrey Grachev](http://cs.angra.pw/)

[Demo](http://cs.angra.pw/templates/activebox/)

## License

Use it freely but do not distribute, sell or reupload elsewhere.

## Credits

### Elegant Line Icon

[Elegant Line Icon](http://www.elegantthemes.com/blog/resources/how-to-use-and-embed-an-icon-font-on-your-website/) by Elegant Themes.

### Font Awesome Icons

[Font Awesome](https://fortawesome.github.io/Font-Awesome/) by Dave Gandy.